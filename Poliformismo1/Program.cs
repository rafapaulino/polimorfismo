﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poliformismo1
{
    class Program
    {
        static void Main(string[] args)
        {
            PessoaFisica pf = new PessoaFisica();
            pf.gravar("Rafael", "Rua do Medo","Lindo","4756874657546",33);

            Console.WriteLine("Nome = " + pf.getNome());
            Console.WriteLine("Endereço = " + pf.getEndereco());
            Console.WriteLine("Apelido = " + pf.getApelido());
            Console.WriteLine("CPF = " + pf.getCPF());
            Console.WriteLine("Idade = " + pf.getIdade());

            Console.ReadKey();
        }
    }
}
