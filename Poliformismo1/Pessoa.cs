﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poliformismo1
{
    class Pessoa
    {
        private string nome;
        private string endereco;

        public void gravar( string nome, string endereco)
        {
            this.nome = nome;
            this.endereco = endereco;

        }

        public string getNome()
        {
            return this.nome;
        }

        public string getEndereco()
        {
            return this.endereco;
        }


    }
}
